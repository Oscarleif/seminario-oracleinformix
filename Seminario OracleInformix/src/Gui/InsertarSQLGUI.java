package Gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import ConexionBD.ConexionBDInformix;
import ConexionBD.ConexionBDOracle;
import Utils.LoopBody;
import Utils.Parallel;
import javax.swing.JLabel;

public class InsertarSQLGUI extends JFrame
	{

		/**
	 * I have no idea what is this for
	 */
		private static final long serialVersionUID = 1L;
		private JPanel contentPane;
		private StartGui mainGui;
		private ConexionBDInformix conexionInformix;
		private ConexionBDOracle conexionOracle;
		private JEditorPane editorPane;
		private JLabel labelNumero = new JLabel("0");

		/**
		 * Create the frame.
		 * @param jFrame 
		 */
		public InsertarSQLGUI(StartGui jFrame)
			{
			addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent arg0) {
					int dialogButton = JOptionPane.YES_NO_OPTION;
					int dialogResult = JOptionPane
							.showConfirmDialog(
									null,
									"�Estas seguro que deseas cerrar?",
									"Alerta", dialogButton);
					if (dialogResult == JOptionPane.YES_OPTION)
						{
							dispose();
							mainGui.getFrmPruebasDeRendimiento().setVisible(true);
							mainGui.getFrmPruebasDeRendimiento().setEnabled(true);
						} else if (dialogResult == dialogButton)
						{
							//frmPruebasDeRendimiento.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
						}
				}
			});
				this.mainGui = jFrame;
				setTitle("Insertar SQL");
				setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
				setLocationRelativeTo(null);
				setBounds(100, 100, 748, 626);
				
				JMenuBar menuBar = new JMenuBar();
				setJMenuBar(menuBar);
				
				
				JMenu mnArchivo = new JMenu("Archivo");
				mnArchivo.setFont(new Font("Segoe UI", Font.PLAIN, 16));
				menuBar.add(mnArchivo);
				
				JMenuItem mntmCerrar = new JMenuItem("Cerrar");
				mnArchivo.add(mntmCerrar);
				
				JMenu mnInformix = new JMenu("Informix");
				mnInformix.setFont(new Font("Segoe UI", Font.PLAIN, 16));
				menuBar.add(mnInformix);
				
				JMenuItem mntmCargarSql = new JMenuItem("Cargar SQL");
				mnInformix.add(mntmCargarSql);
				mntmCargarSql.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						cargarArchivoSQLInformix();
				}});
				
				JMenu mnOracle = new JMenu("Oracle");
				mnOracle.setFont(new Font("Segoe UI", Font.PLAIN, 16));
				menuBar.add(mnOracle);
				
				JMenuItem mntmCargarSql_1 = new JMenuItem("Cargar SQL");
				mntmCargarSql_1.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						//ClickItem para insertar SQL Oracle
						cargarSQLOracle();
					}
				});
				mnOracle.add(mntmCargarSql_1);
				
				JMenu mnAyuda = new JMenu("Ayuda");
				mnAyuda.setFont(new Font("Segoe UI", Font.PLAIN, 16));
				menuBar.add(mnAyuda);
				
				JMenuItem mntmQueEsEsto = new JMenuItem("Que es esto ?");
				mnAyuda.add(mntmQueEsEsto);
				contentPane = new JPanel();
				contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
				setContentPane(contentPane);
				contentPane.setLayout(null);
				
				JScrollPane scroll = new JScrollPane();
				scroll.setBounds(12, 13, 709, 469);
				
				
				editorPane = new JEditorPane();
				editorPane.setBounds(12, 13, 709, 469);
				scroll.setViewportView(editorPane);
				contentPane.add(scroll);
				
				JButton btnInsertarSql = new JButton("Insertar SQL");
				btnInsertarSql.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						//ClickButton para insertar SQL Informix
						try
							{
								insertarDatosSQL();
							} catch (SQLException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						
					}
				});
				btnInsertarSql.setFont(new Font("Tahoma", Font.PLAIN, 15));
				btnInsertarSql.setBounds(12, 516, 136, 25);
				contentPane.add(btnInsertarSql);
				
				JButton btnCancelar = new JButton("Cancelar");
				btnCancelar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						int dialogButton = JOptionPane.YES_NO_OPTION;
						int dialogResult = JOptionPane
								.showConfirmDialog(
										null,
										"�Estas seguro que deseas cerrar?",
										"Alerta", dialogButton);
						if (dialogResult == JOptionPane.YES_OPTION)
							{
								dispose();
								mainGui.getFrmPruebasDeRendimiento().setVisible(true);
								mainGui.getFrmPruebasDeRendimiento().setEnabled(true);
							} else if (dialogResult == dialogButton)
							{
								//frmPruebasDeRendimiento.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
							}
						
					}
				});
				btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 15));
				btnCancelar.setBounds(609, 516, 112, 25);
				contentPane.add(btnCancelar);
				
				JLabel lblInserciones = new JLabel("Inserciones :");
				lblInserciones.setFont(new Font("Tahoma", Font.PLAIN, 16));
				lblInserciones.setBounds(188, 523, 101, 14);
				contentPane.add(lblInserciones);
				
				//JLabel labelNumero = new JLabel("0");
				labelNumero.setFont(new Font("Tahoma", Font.PLAIN, 16));
				labelNumero.setBounds(299, 523, 46, 14);
				contentPane.add(labelNumero);
			}

		public ConexionBDInformix getConexionInformix()
			{
				return conexionInformix;
			}
		public void setConexionInformix(ConexionBDInformix conexionInformix)
			{
				this.conexionInformix = conexionInformix;
			}
		public ConexionBDOracle getConexionOracle()
			{
				return conexionOracle;
			}
		public void setConexionOracle(ConexionBDOracle conexionOracle)
			{
				this.conexionOracle = conexionOracle;
			}
		/**
		 * Insertar datos a las bases de datos.
		 * Este metodo lee el texto dentro del campo de texto.
		 * Para hacer las inserciones dentro de los motores.
		 * @throws SQLException 
		 */
		private void insertarDatosSQL() throws SQLException
		{
			String[] lines = editorPane.getText().split("\\n");
			/*
			for(int i=0;i<lines.length;i++)
			{
				String sqlString = lines[i];
				//conexionInformix.ejecutarUpdate(sqlString);
				//conexionInformix.getConnection().commit();
				conexionOracle.executeUpdateStatement(sqlString);
				System.out.println(sqlString);
			}
			*/
			long startTime = System.nanoTime();
			
			Parallel.For(0, lines.length, new LoopBody<Integer>() {
int count= 0;
				@Override
				public void run(Integer i) {
					// TODO Auto-generated method stub
					String sqlString = lines[i];
					try
						{
							conexionInformix.ejecutarUpdate(sqlString);
							count++;
							if(count == 32000)
							{
								conexionInformix.getConnection().commit();
								count=0;
							}
						} catch (SQLException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
					System.out.println(i);
				}

			});
			long endTime = System.nanoTime();
			System.out.println("The complete time is: " + (double)(endTime-startTime)/(Math.pow(10, 9)) + " seconds");
			conexionInformix.getConnection().commit();
			System.out.println("El comit esta completo");
		}
		protected void cargarArchivoSQLInformix()
		{
			
			 JFileChooser chooser = new JFileChooser();
		        int returnName = chooser.showOpenDialog(null);
		        String path;
		        if (returnName == JFileChooser.APPROVE_OPTION) {
		            File f = chooser.getSelectedFile();
		            if (f != null) {
		                path = f.getAbsolutePath();	
						try(BufferedReader br = new BufferedReader(new FileReader(f.getAbsolutePath() ))) {
							int count=0;
							int insCount = 0;
						    for(String line; (line = br.readLine()) != null; ) {
						        // process the line.
						    	count++;
						    	insCount++;
						    	try
									{
										if(count == 32695)
											{
												conexionInformix.getConnection().commit();
												//conexionInformix.cerrarConeccion();
												mainGui.cargarConexiones();
												count = 0;
												labelNumero.setText(Integer.toString(insCount));
											}
										conexionInformix.ejecutarUpdate(line);
										System.out.println(insCount);
										
										
									} catch (SQLException e)
									{
										// TODO Auto-generated catch block
										System.out.println("SQL Exception Fails");
										e.printStackTrace();
										conexionInformix.cerrarConeccion();
										break;
									}
						    	catch (NullPointerException e) {
						    		System.out.println("No hay Conexiones");
						    		break;
								}
						    }
						    conexionInformix.getConnection().commit();
						    conexionInformix.cerrarConeccion();
						    // line is not visible here.
						} catch (FileNotFoundException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SQLException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
									System.out.println("Seconds SQL Exeption");
								}
						
					}
		            }
		        }///Fin del metodo cargarSQLInformix
		
		protected void cargarSQLOracle() {
			JFileChooser chooser = new JFileChooser();
	        int returnName = chooser.showOpenDialog(null);
	        String path;
	        if (returnName == JFileChooser.APPROVE_OPTION) {
	            File f = chooser.getSelectedFile();
	            if (f != null) {
	                path = f.getAbsolutePath();	
	                String file = path;
					try(BufferedReader br = new BufferedReader(new FileReader(file ))) {
						int count=0;
						int realCount = 0;
					    for(String line; (line = br.readLine()) != null; ) {
					        // process the line.
					    	count++;
					    	realCount++;
					    	try
								{
									if(count == 10000)
										{
											conexionOracle.getConnection().commit();
											//conexionInformix.cerrarConeccion();
											//conexionOracle.closeConnection();
											//mainGui.cargarConexiones();
											count = 0;
										}
									String sqlCommand = line.substring(0, line.length()-1);
									conexionOracle.executeUpdateStatement(sqlCommand);
									System.out.println(realCount);
								} catch (SQLException e)
								{
									// TODO Auto-generated catch block
									System.out.println("SQL Exception Fails");
									e.printStackTrace();
									conexionOracle.closeConnection();
									break;
								}
					    	catch (NullPointerException e) {
					    		System.out.println("No hay Conexiones");
					    		break;
							}
					    }
					    conexionOracle.getConnection().commit();
					    conexionOracle.closeConnection();
					    // line is not visible here.
					} catch (FileNotFoundException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SQLException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
								System.out.println("Seconds SQL Exeption");
							}
					
				}
	            }
			
		}
		}
	
