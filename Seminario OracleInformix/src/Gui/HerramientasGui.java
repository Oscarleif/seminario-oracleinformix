package Gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class HerramientasGui extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private StartGui gui;
	private JTextField textFieldInformix;
	private JTextField textFieldOracle;

	/**
	 * Create the frame.
	 * 
	 * @param actionListener
	 */
	public HerramientasGui(ActionListener actionListener) {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane
						.showConfirmDialog(
								null,
								"�Estas seguro que deseas cerrar?",
								"Alerta", dialogButton);
				if (dialogResult == JOptionPane.YES_OPTION)
					{
						dispose();
					} else if (dialogResult == JOptionPane.NO_OPTION)
					{
						System.out.println("Cancel the close window ");
					}
			}
		});
		setTitle("Herramientas");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 556, 303);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnArchivo = new JMenu("Archivo");
		mnArchivo.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		menuBar.add(mnArchivo);

		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnArchivo.add(mntmGuardar);

		JMenuItem mntmSalir = new JMenuItem("Salir");
		mnArchivo.add(mntmSalir);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblDireccionIpDe = new JLabel("Direccion IP de informix:");
		lblDireccionIpDe.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDireccionIpDe.setBounds(10, 44, 167, 14);
		contentPane.add(lblDireccionIpDe);

		JLabel lblDireccionIpDe_1 = new JLabel("Direccion IP de oracle:");
		lblDireccionIpDe_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDireccionIpDe_1.setBounds(10, 92, 167, 14);
		contentPane.add(lblDireccionIpDe_1);

		textFieldInformix = new JTextField();
		textFieldInformix.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldInformix.setBounds(189, 41, 337, 20);
		contentPane.add(textFieldInformix);
		textFieldInformix.setColumns(10);

		textFieldOracle = new JTextField();
		textFieldOracle.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldOracle.setColumns(10);
		textFieldOracle.setBounds(187, 91, 339, 20);
		contentPane.add(textFieldOracle);

		JButton btnNewButton = new JButton("Guardar");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String oracle = textFieldOracle.getText();
				String informix = textFieldInformix.getText();
				
				gui.setIpInformix(informix);
				gui.setIpOracle(oracle);
				
				try {
					PrintWriter writer=new PrintWriter("InformixOracle.txt","UTF-8");
					writer.println(informix);
					writer.println(oracle);
					writer.close();
					gui.getFrmPruebasDeRendimiento().setEnabled(true);
					gui.getFrmPruebasDeRendimiento().setVisible(true);;
					dispose();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnNewButton.setBounds(10, 191, 89, 23);
		contentPane.add(btnNewButton);

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				gui.getFrmPruebasDeRendimiento().setEnabled(true);
				gui.getFrmPruebasDeRendimiento().setVisible(true);
			}
		});
		btnCancelar.setBounds(437, 191, 89, 23);
		contentPane.add(btnCancelar);


	}

	public void linkStartGui(StartGui gui) {
		this.gui = gui;
		gui.getFrmPruebasDeRendimiento().setEnabled(false);

	}
	public void cargarIP()
	{
		
		if(gui.getIpInformix() != null)
		{
			textFieldInformix.setText(gui.getIpInformix());
		}
		if(gui.getIpOracle() != null)
		{
			textFieldOracle.setText(gui.getIpOracle());
		}
	}
}
