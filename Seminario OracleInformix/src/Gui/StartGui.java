package Gui;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import ConexionBD.ConexionBDInformix;
import ConexionBD.ConexionBDOracle;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

import java.awt.Font;
import javax.swing.JEditorPane;


public class StartGui {

	private JFrame frmPruebasDeRendimiento;
	private String ipInformix;
	private String ipOracle;
	private ConexionBDInformix conexionInformix;
	private ConexionBDOracle conexionOracle;
	public static String ESTADO_CONEXION_ORACLE = "Conexion solo Oracle, ip informix ?";
	public static String ESTADO_CONEXION_INFORMIX = "Conexion solo Informix, ip de oracle ?";
	public static String ESTADO_CONEXION_DUAL = "bases de datos sincronizadas";
	public static String ESTADO_DESCONECTADO = "No hay conexiones, revisa las IP";
	/**
	 * Launch the application.
	 */	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StartGui window = new StartGui();
					window.frmPruebasDeRendimiento.setVisible(true);
					window.frmPruebasDeRendimiento.setLocationRelativeTo(null);
					window.cargarIp();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public StartGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPruebasDeRendimiento = new JFrame();
		frmPruebasDeRendimiento.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane
						.showConfirmDialog(
								null,
								"�Estas seguro que deseas cerrar?",
								"Alerta", dialogButton);
				if (dialogResult == JOptionPane.YES_OPTION)
					{
						System.exit(1);
					} else if (dialogResult == dialogButton)
					{
						frmPruebasDeRendimiento.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
					}
			}
		});
		frmPruebasDeRendimiento.setTitle("Pruebas de Rendimiento");
		frmPruebasDeRendimiento.setBounds(100, 100, 717, 535);
		frmPruebasDeRendimiento.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		JButton btnConsultar = new JButton("Consultar");
		btnConsultar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Trabajo con el empleado !!!!!!!!!!!!!!!!!!!!
				
				//String query = "select * from cliente where";
				//String query = "select * from cliente where ID_CLIENTE = "+ darRandom();
				String query = "select * from CLIENTE where rownum < 250000";
				
				long startTime = System.nanoTime();
				try {
					conexionInformix.ejecutarQuery(query);
				} catch (NullPointerException exeption) {
					// TODO: handle exception
				} catch (SQLException e1)
					{
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				System.out.println(query);
				long stopTime = System.nanoTime();
				double duration = (double)(stopTime - startTime) / (Math.pow(10, 9));
				//System.out.println("\n"+"Time in nano Seconds: " + Long.toString(stopTime-startTime));
				System.out.println(duration + " seconds, to finish informix");
				
				long startTimeO = System.nanoTime();
				conexionOracle.executeQueryStatement(query);
				long stopTimeO = System.nanoTime();
				double durationO = (double) (stopTimeO - startTimeO)/(Math.pow(10, 9));
				System.out.println("\n");
				System.out.println(durationO + " seconds, to finish oracle" );
			}
		});
		
		JLabel lblEstadoConexion = new JLabel("Estado conexion :");
		lblEstadoConexion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JLabel lblEstado = new JLabel("Estado");
		lblEstado.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JLabel lblConsultaSql = new JLabel("Consulta SQL:");
		lblConsultaSql.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JEditorPane editorPane = new JEditorPane();
		editorPane.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JLabel lblResultadoSql = new JLabel("Resultado SQL:");
		lblResultadoSql.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JEditorPane editorPane_1 = new JEditorPane();
		editorPane_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JButton btnSqlConsulta = new JButton("Consulta SQL");
		btnSqlConsulta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String sqlCommand = editorPane.getText();
				String[] lines = sqlCommand.split(System.getProperty("line.separator"));
				String result = "";
				try
					{
						
						long startTime = System.nanoTime();
						conexionInformix.ejecutarQuery(lines[0]);
						long endTime = System.nanoTime();
						double totalTime = (double)(endTime-startTime)/(Math.pow(10, 9));
						String resultI = "Informix: "+Double.toString(totalTime) + " Seconds";
						result = resultI + "\n";
						//editorPane_1.setText(resultI);
					} catch (SQLException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				long startTime = System.nanoTime();
				conexionOracle.executeQueryStatement(lines[1]);
				long endTime = System.nanoTime();
				double totalTime = (double)(endTime-startTime)/(Math.pow(10, 9));
				String resultO = "Oracle: "+Double.toString(totalTime) + " Seconds";
				result = result + resultO;
				editorPane_1.setText(result);
			}
		});
		btnSqlConsulta.setFont(new Font("Tahoma", Font.PLAIN, 15));
		GroupLayout groupLayout = new GroupLayout(frmPruebasDeRendimiento.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnSqlConsulta)
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
							.addComponent(lblEstadoConexion)
							.addGap(18)
							.addComponent(lblEstado, GroupLayout.PREFERRED_SIZE, 281, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 141, Short.MAX_VALUE)
							.addComponent(btnConsultar))
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
							.addComponent(lblResultadoSql)
							.addGap(10)
							.addComponent(editorPane_1, 0, 0, Short.MAX_VALUE))
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
							.addComponent(lblConsultaSql)
							.addGap(18)
							.addComponent(editorPane, GroupLayout.PREFERRED_SIZE, 544, GroupLayout.PREFERRED_SIZE)))
					.addGap(17))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(30)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblConsultaSql)
						.addComponent(editorPane, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE))
					.addGap(54)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblResultadoSql)
						.addComponent(editorPane_1, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(btnSqlConsulta)
					.addGap(91)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEstado)
						.addComponent(btnConsultar)
						.addComponent(lblEstadoConexion))
					.addContainerGap())
		);
		frmPruebasDeRendimiento.getContentPane().setLayout(groupLayout);
		
		JMenuBar menuBar = new JMenuBar();
		frmPruebasDeRendimiento.setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		mnArchivo.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		menuBar.add(mnArchivo);
		
		JMenuItem mntmConectar = new JMenuItem("Conectar");
		mntmConectar.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		mntmConectar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cargarConexiones();
                boolean informix = conexionInformix.testConeccion();
                boolean oracle = conexionOracle.testConecction();
                if(conexionInformix.testConeccion() && conexionOracle.testConecction())
                {
                    lblEstado.setText(ESTADO_CONEXION_DUAL);
                }
                else if (!conexionInformix.testConeccion() && conexionOracle.testConecction())
                {
                    lblEstado.setText(ESTADO_CONEXION_ORACLE);
                }
                else if (conexionInformix.testConeccion() && !conexionOracle.testConecction())
                {
                    lblEstado.setText(ESTADO_CONEXION_INFORMIX);
                }
                else if(!conexionInformix.testConeccion() && !conexionOracle.testConecction())
                {
                    lblEstado.setText(ESTADO_DESCONECTADO);
                }

				
			}
		});
		mnArchivo.add(mntmConectar);
		
		JMenuItem mntmInsertar = new JMenuItem("Insertar");
		mntmInsertar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Item de Insertar Datos Gui.
				InsertarSQLGUI sqlInsert = new InsertarSQLGUI(StartGui.this);
				sqlInsert.setLocationRelativeTo(null);
				getFrmPruebasDeRendimiento().setEnabled(false);
				sqlInsert.setVisible(true);
				sqlInsert.setConexionInformix(conexionInformix);
				sqlInsert.setConexionOracle(conexionOracle);
				
			}
		});
		mntmInsertar.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		mnArchivo.add(mntmInsertar);
		
		JMenuItem mntmCerrar = new JMenuItem("Cerrar");
		mntmCerrar.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		mnArchivo.add(mntmCerrar);
		
		JMenu mnNewMenu_1 = new JMenu("Editar");
		mnNewMenu_1.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmPreferencias = new JMenuItem("Preferencias");
		mntmPreferencias.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		mntmPreferencias.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				HerramientasGui j = new HerramientasGui(this);
				j.setVisible(true);
				j.setLocationRelativeTo(null);
				j.linkStartGui(StartGui.this);
				j.cargarIP();
			}
		});
		mnNewMenu_1.add(mntmPreferencias);
		
		JMenu mnGenerarSql = new JMenu("Generar SQL");
		mnGenerarSql.setFont(new Font("Segoe UI", Font.PLAIN, 18));
		menuBar.add(mnGenerarSql);
		
		JMenuItem mntmConsultaMenorQue = new JMenuItem("Consulta menor que.");
		mntmConsultaMenorQue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String consultaMenor = "SELECT FIRST 1 * FROM cliente "+System.getProperty("line.separator") +"SELECT *  FROM cliente where rownum < 1";
				editorPane.setText(consultaMenor);
			}
		});
		mnGenerarSql.add(mntmConsultaMenorQue);
		
		JMenuItem mntmConsultaIdAleatorio = new JMenuItem("Consulta ID aleatorio");
		mntmConsultaIdAleatorio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String query = "select * from cliente where ID_CLIENTE = "+ darRandom();
				editorPane.setText(query+System.getProperty("line.separator")+query);
			}
		});
		mnGenerarSql.add(mntmConsultaIdAleatorio);
		
		
	}
	public void crearConexionInformix()
	{
		conexionInformix = new ConexionBDInformix();
		try {
			conexionInformix.cargarDriver();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		conexionInformix.crearConexion(ipInformix);
	}
	public void crearConexionOracle()
	{
		conexionOracle = new ConexionBDOracle(ipOracle);
	}
	public void cargarIp() throws IOException
	{
		BufferedReader br = null;
		try {
			
			FileReader file = new FileReader("InformixOracle.txt");
			
		 br = new BufferedReader(file);
		 
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();
		        ArrayList<String> ips = new ArrayList<String>();

		        while (line != null) {
		            sb.append(line);
		            sb.append(System.lineSeparator());
		            ips.add(line);
		            line = br.readLine();
		            
		        }
		        String everything = sb.toString();
		        setIpInformix(ips.get(0).toString());
		        setIpOracle(ips.get(1).toString());

		    }catch(NullPointerException ex)
		{
		    	System.out.println("Una de las IP no esta guardada");
		}
		finally {
		        br.close();
		    }
	}
	

	public JFrame getFrmPruebasDeRendimiento() {
		return frmPruebasDeRendimiento;
	}

	public void setFrmPruebasDeRendimiento(JFrame frmPruebasDeRendimiento) {
		this.frmPruebasDeRendimiento = frmPruebasDeRendimiento;
	}

	public String getIpInformix() {
		return ipInformix;
	}

	public void setIpInformix(String ipInformix) {
		this.ipInformix = ipInformix;
	}

	public String getIpOracle() {
		return ipOracle;
	}

	public void setIpOracle(String ipOracle) {
		this.ipOracle = ipOracle;
	}
	public void cargarConexiones()
	{
		crearConexionInformix();
		crearConexionOracle();
	}
	public int darRandom()
	{
		Random r = new Random();
		int Low = 0;
		int High = 5000000;
		int R = r.nextInt(High-Low) + Low;
		return R;
	}
}
