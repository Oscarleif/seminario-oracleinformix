/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ConexionBD;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 * @author Andres Mauricio
 * Actualizado Oscar Leif
 */
public class ConexionBDOracle {
    public final static String CLASE_CONEXION = "oracle.jdbc.driver.OracleDriver";

    private String ipAdress;
    private String user;
    private String password;
    private String url;
    private Connection conn;
    private Statement stmt;
    /**
     * This is a default constructor, 
     */
    public ConexionBDOracle(){
        url = "jdbc:oracle:thin:@192.168.1.104:1521:xe";
        user = "oscar";
        password = "oscar";//The default or Old password was ORACLE
        conectar();
    }
    public ConexionBDOracle(String ip)
    {
    	url = "jdbc:oracle:thin:@"+ip+":1521:xe";
        user = "oscar";
        password = "oscar";//The default or Old password was ORACLE
        conectar();
    }

    public ConexionBDOracle(String ipadd, String us, String pass, String u){
        ipAdress = ipadd;
        user = us;
        password = pass;
        url = u;
        conectar();
    }
        ///Comment
    private void conectar(){
        try{
            Class.forName( CLASE_CONEXION ).newInstance();
            DriverManager.setLoginTimeout(0x2);
            
            //conn.setNetworkTimeout(null, 5000);
            conn = DriverManager.getConnection(url, user, password);
            //Statement s = conn.createStatement();
            //s.execute("alter session set NLS_DATE_FORMAT='YYYY-MM-dd'");
            stmt = conn.createStatement();
            stmt.execute("alter session set NLS_DATE_FORMAT='YYYY-MM-dd'");
            stmt.close();
            conn.setAutoCommit(false);
            //JOptionPane.showMessageDialog(null,"Se conecto a la base de datos Oracle " , "Mensajes",JOptionPane.INFORMATION_MESSAGE);
        }
        catch(SQLException ex){
            //JOptionPane.showMessageDialog(null,"Se genero el siguiente error " + ex, "Mensajes",JOptionPane.ERROR_MESSAGE);
            System.out.println(ex);
            System.err.println("No hay Conexion de Oracle");
        }
        catch(ClassNotFoundException ex){
           // JOptionPane.showMessageDialog(null,"Se genero el siguiente error " + ex, "Mensajes",JOptionPane.ERROR_MESSAGE);
            System.out.println("Clase no encontrada: " + ex);
        }
        catch(Exception ex){
            //JOptionPane.showMessageDialog(null,"Se genero el siguiente error " + ex, "Mensajes",JOptionPane.ERROR_MESSAGE);
            System.out.println("Otra excepcion: " + ex);
        }
    }

     public boolean executeUpdateStatement(String cad){
        int r = 0;
        try{
            stmt = conn.createStatement();
            r = stmt.executeUpdate(cad);
            //System.out.println("Actualizacion realizada... "+r);
            //conn.commit();
            stmt.close();
            //stmt = null;
            return true;
        }
        catch(Exception ex){
            System.out.println("\n"+"No se pudo efecutar la grabacion... "+ex);
            System.err.println("\n "+ cad);
            return false;
        }
    }

     public ResultSet executeQueryStatement(String cad){
        ResultSet res = null;
        try{
            stmt = conn.createStatement();
            res= stmt.executeQuery(cad);
            System.out.println("Consulta realizada...");
        }
        catch(Exception ex){
                System.out.println("No se pudo efectuar la consulta... "+ex);
        }
        return res;
    }

    public void closeConnection(){
        try{
            conn.close();
        }
        catch(SQLException e){
        }
    }
    
    /**
     * 
     * @return el objeto conexion de oracle
     */
    public Connection getConnection()
    {
        return conn;
    }
    /**
     * testConnection usando un simple query en java se puede conocer el estado de
     *la connecion de oracle. Retorna true si esta se puede connectar a oracle, false
     *si esta no se pudo connectar.
     *
     */ 
    public boolean testConecction()
    {
        boolean conectado = false;
        String validationQuery = "Select 1 from dual";
        ResultSet result=null;
        try {
            stmt = conn.createStatement();
            result = stmt.executeQuery(validationQuery);
            conectado = true;
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBDOracle.class.getName()).log(Level.SEVERE, null, ex);
            return conectado = false;
        }
        catch(Throwable e)
        {
            System.err.println("No hay conexiones con Oracle");
        }
        return conectado;
        
    }

}