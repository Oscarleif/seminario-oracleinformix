/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConexionBD;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.Query;
import javax.swing.JOptionPane;

/**
 * Esta clase es un ayudante para la conexion de informix.
 * su unica funcion es conocer el estado de la conexion para poder 
 * ejecutar consultas actualizaciones eliminaciones y registros
 * @author OscarLeif
 */
public class ConexionBDInformix {
    
    private Connection connection;
    private Statement sentencia;

    public void cargarDriver() throws ClassNotFoundException {

        try {

            Class.forName("com.informix.jdbc.IfxDriver");
            //JOptionPane.showMessageDialog(null, "Se cargo el driver de Informix. \n" , "Atención", JOptionPane.INFORMATION_MESSAGE);
            System.out.println("Se conecto perfectamente con el Informix JDBC driver");
        } catch (ClassNotFoundException excepcion) {
            //JOptionPane.showMessageDialog(null, "ERROR: falló la carga del Informix JDBC driver. \n" + excepcion.getMessage(), "Atención", JOptionPane.INFORMATION_MESSAGE);
            System.out.println("ERROR: fallo la carga del Informix JDBC driver. \\n");
            return;
        }

    }
    
    public void crearConexion(String nombreMaquina)
    {
        try {
            DriverManager.setLoginTimeout(2);
            //connection.setNetworkTimeout(null, 5000);
            connection = DriverManager.getConnection("jdbc:informix-sqli://" + nombreMaquina + ":9088/" + "videotienda" + ":INFORMIXSERVER=" + "videotienda" + ";user=" + "informix" + ";password=" + "informix");
            connection.setAutoCommit(false);

            //JOptionPane.showMessageDialog(null,"Coneccion satisfactoria a informix " , "Mensajes",JOptionPane.INFORMATION_MESSAGE);
            System.out.printf("Se creo la coneccion a la base de datos de forma satisfactoria");

        } catch (SQLException excepcion) {

            //JOptionPane.showMessageDialog(null, "ERROR: No se pudo establecer la conexión con la base. \n" + excepcion.getMessage(), "Atención", JOptionPane.INFORMATION_MESSAGE);
            System.out.println("ERROR: No se pudo establecer la conexion con la base de datos Informix.");
            System.out.println("Cargaste el Driver ?");
            return;

        } 
        catch(NullPointerException nul)
                {
                    
                }
    }

    public void crearConexion(String nombreMaquina, String nombreBase, String nombreInstancia, String nombreUsuario, String passwordUsuario) throws SQLException {
        ///Comment
        try {
            DriverManager.setLoginTimeout(2);
            //connection.setNetworkTimeout(null, 5000);
            connection = DriverManager.getConnection("jdbc:informix-sqli://" + nombreMaquina + ":9088/" + nombreBase + ":INFORMIXSERVER=" + nombreInstancia + ";user=" + nombreUsuario + ";password=" + passwordUsuario);
            connection.setAutoCommit(false);

            //JOptionPane.showMessageDialog(null,"Coneccion satisfactoria a informix " , "Mensajes",JOptionPane.INFORMATION_MESSAGE);
            System.out.printf("Se creo la coneccion a la base de datos de forma satisfactoria");

        } catch (SQLException excepcion) {

            //JOptionPane.showMessageDialog(null, "ERROR: No se pudo establecer la conexión con la base. \n" + excepcion.getMessage(), "Atención", JOptionPane.INFORMATION_MESSAGE);
            System.out.println("ERROR: No se pudo establecer la conexion con la base.");
            return;

        } 
        catch(NullPointerException nul)
                {
                    
                }

    }
    /*
     * Este metodo es solo para hacer consultas espeficas en la base de datos
     * de lo contrario este lanzara una excepcion
     * @param sentenciaPasada
     * @return
     * @throws SQLException 
     */
    public ResultSet ejecutarQuery(String sentenciaPasada) throws SQLException {

    	ResultSet resultado = null;
    	
        try {
            
            PreparedStatement sentenciaPreparada = null;

            //ResultSetMetaData metaResultado = null;

            sentenciaPreparada = connection.prepareStatement(sentenciaPasada, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

            resultado = sentenciaPreparada.executeQuery();

            //metaResultado = resultado.getMetaData();
            

        } catch (SQLException excepcion) {

            //JOptionPane.showMessageDialog(null, "ERROR: fallo la ejecucion de la sentencia. \n" + excepcion.getMessage(), "Atencion", JOptionPane.INFORMATION_MESSAGE);

            System.out.println("\n ERROR: " + excepcion.getMessage());
            return null;

        }
        catch(NullPointerException e)
        {
            System.err.println("No esta activa informix");
        }
            return resultado;
    }
    
    /**
     * Este metodo es para hacer registros, eliminaciones, actualizaciones
     * en la base de datos, por lo tanto los Querys o consultas aqui no funcionan
     * @param sentenciaPasada
     * @throws SQLException 
     */

    public void ejecutarUpdate(String sentenciaPasada) throws SQLException {

//aquí la variable "sentencia" pasada como parámetro, debe ser del tipo SQL.
//aqui funcionan los inserts
        try {
            PreparedStatement sentenciaPreparada = null;

            sentenciaPreparada = connection.prepareStatement(sentenciaPasada, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            //no commit
            sentenciaPreparada.executeUpdate();//Here commits cannot change after this work
            sentenciaPreparada.close();
            
        } catch (SQLException excepcion) {

            //JOptionPane.showMessageDialog(null, "ERROR: fallo la ejecucion de la sentencia. \n" + excepcion.getMessage(), "Atencion", JOptionPane.INFORMATION_MESSAGE);
            System.err.println("ExepcionSQL: "+sentenciaPasada);
            System.out.println("ERROR: " + excepcion.getMessage());
        } catch (NullPointerException e) {
			System.err.println("No hay conexion con Informix");
		}

    }
     /**
      * Cierra la conexion de la base de datos
      */
    public void cerrarConeccion()
    {
        if(connection != null)
        {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(ConexionBDInformix.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
    * Este metodo hace un pequeño query para conocer el estado de la coneccion 
    * retorna true si esta se pudo conectar, de lo contrario retorna false;
    */
    public boolean testConeccion() {
        String query = "select count(*) from systables";
        PreparedStatement sentenciaPreparada = null;
        boolean conectado = false;
        try {
            ResultSet resultado = null;            

            ResultSetMetaData metaResultado = null;

            sentenciaPreparada = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            resultado = sentenciaPreparada.executeQuery();
            metaResultado = resultado.getMetaData();
            conectado=true;
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBDInformix.class.getName()).log(Level.SEVERE, null, ex);
            return conectado;
        }
        catch(NullPointerException e)
        {
            
        }
        
        return conectado;

    }
    /**
     * Retorna la coneccion de informix para posteriormente realizar transacciones
     * @return 
     */
    public Connection getConnection()
    {
        return connection;
    }
        
}
